from . import LogDatei
from . import Zeitgeber


class WatchDog:
    def __init__(self, fname, fnlog):
        self.fname = fname    # Dateiname der Watch-Dog-Datei
        self.fnlog = fnlog    # Dateiname des Fehler-Logs
        self.log = LogDatei.LogDatei(self.fnlog)

    def watch(self):
        try:
            file = open(self.fname, "w")
            file.write("Ich bin noch da\n")
            file.close()
        except:
            meld = "Watchdatei ließ sich nicht schreiben"
            print(meld)
            self.log.write_log(meld)
            return
        meld = 'Watchdog zurückgesetzt'
        print(meld)
        # self.log.write_log(meld)   # später auskommentieren


if __name__ == "__main__":
    zeit1 = Zeitgeber(10, True, True)
    zeit2 = Zeitgeber(13, True, True)
    dateiLog = '/home/pi/test.log'
    log = LogDatei(dateiLog)
    dateiWatch = '/mnt/RAMDisk/test.log'
    watch = WatchDog(dateiWatch, dateiLog)

    while True:
        if zeit1.check():
            print('eine Meldung')
            print(log.write_log('eine Meldung'))
        if zeit2.check():
            print('Watch ausgelöst')
            watch.watch()





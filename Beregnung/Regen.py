from Beregnung import Anlage
from Beregnung import I2Cio
from Beregnung import Zeitgeber
import Beregnung as br
from typing import Callable


class Regen:
    musterFertig = True     # alle Plätze des Musters abgearbeitet

    # Beregnung, Zeiten werden über die Schleife im Hauptprogramm gewährleistet
    def __init__(self, wasserZeit : Zeitgeber.Zeitgeber, sperrZeit : Zeitgeber.Zeitgeber, nochZeit : Zeitgeber.Zeitgeber):
        self.konf = Anlage.AnlagenKonfiguration
        self.stat = Anlage.AnlagenStatus
        self.wasserZeit = wasserZeit    # Zeitgeberobjekt für Wasser an
        self.sperrZeit = sperrZeit      # Zeitgeberobjekt für Wasser geperrt
        self.nochZeit = nochZeit        # Zeitgeberobjekt für Countdown der Restlaufzeit
        self.i2c = I2Cio
        self.taster = [False] * self.konf.nTaster                 # Taster ist gedrückt: True
        self.tasterSperre = [False] * (self.konf.nBits + 1)       # gesperrter Eingang für eine gewisse Zeit (z.B. 5s)
        self.wasser = False         # Wasser läuft bzw. Sperrzeit
        self.wasserPlatz = 1        # Wasser läuft auf diesem Platz bzw.
        self.iPlatz = 1             # Nummer des Platzes, der gerade automatisch bewässert wird

    def beregnung_ein(self, platz_nr : int, t_ein : float):   # starte eine Beregnung
        # Zeitgeber (wasserZeit) mit t_ein starten
        self.wasser = True
        self.wasserPlatz = platz_nr
        print('Zeitgebern mit t_ein gestartet ', t_ein)
        self.sperrZeit.aktiv = False    # sorge dafür, dass der Sperrzeitzähler noch nicht losläuft
        self.wasserZeit.zeit = t_ein
        self.wasserZeit.start()
        # Ventil und Led ein
        print('Ventil und Led ein, Platz Nr.: ', platz_nr)
        self.i2c.ein(self.konf.ausgang[self.wasserPlatz][0], self.konf.ausgang[self.wasserPlatz][1])
        self.i2c.ein(self.konf.Led[self.wasserPlatz][0], self.konf.Led[self.wasserPlatz][1])
        self.stat.anlageStatus[self.wasserPlatz - 1] = True         # Led-Status in globaler Variabler
        self.stat.t_noch = self.stat.tRegen + self.konf.t_sperre    # Countdown der Laufzeit vorbereiten
        self.nochZeit.aktiv = True      # Countdown der Laufzeit aktiv
        br.update_schalter()            # zur Kommunikation mit Weboberfläche

    def wasser_ende(self):   # macht das Wasser zu
        # nach Ablauf von 'wasserZeit' wird das Ventil geschlossen
        self.i2c.aus(self.konf.ausgang[self.wasserPlatz][0], self.konf.ausgang[self.wasserPlatz][1])
        # Zeitgeber für t_sperre starten
        self.sperrZeit.start()
        return

    def beregnung_ende(self):   # gibt Beregnung wieder frei
        # nach Ablauf von 'sperrZeit' wird die Bewässerung wieder freigegebn
        self.i2c.aus(self.konf.Led[self.wasserPlatz][0], self.konf.Led[self.wasserPlatz][1])
        self.stat.anlageStatus[self.wasserPlatz - 1] = False  # Led-Status in globaler Variabler
        self.nochZeit.aktiv = False             # Anzeige der Laufzeit nicht aktiv
        br.update_schalter()
        self.wasser = False
        print('Beregnung ist zuende')
        return

    def platz_schalten(self, nr):
        if self.wasser and nr == self.wasserPlatz:
            print('Platz ', nr, ' wird nicht mehr bewässert')
            self.wasser_ende()
        if not self.wasser:
            # wenn der Platz nicht gesperrt ist
            if Anlage.AnlagenStatus.muster[nr - 1]:
                print('Platz ', nr, ' wird bewässert')
                self.beregnung_ein(nr, Anlage.AnlagenStatus.tRegen)

    def rundlauf(self):    # hier werde die Parameter für den automatischen Ablauf / Rundlauf gesetzt
        # laufende Beregnung stoppen bei Rundlauf ein
        self.wasser_ende()
        self.iPlatz = 1  # Nummer des Platzes, der gerade automatisch bewässert wird
        Regen.musterFertig = False

    def leer(self):
        pass

    def tastenAktion(self, tasteNr: int, tasteTxt: str, zeit: list, func1: Callable, func2: Callable):
        # Aktionen unabhängig vom Automatikbetrieb, komplett als Methode in Regen realisiert
        # beim ersten Drücken wird func2() ausgeführt
        if tasteNr-1 == self.konf.taster[tasteTxt]:
            # ggf. Sperrzeit z.B. 5s für den Taster, wichtig für kleinere Prellzeiten
            if not self.tasterSperre[tasteNr]:
                self.tasterSperre[tasteNr] = True   # Sperre wird nach tSperr wieder aufgehoben
                # für jeden Eingang brauche ich einen eingenen Zeitgeber
                zeit[tasteNr].start()     # Zugriff auf Objekt in 'Main' über 'zeit'
                sperrIndex = self.konf.taster[tasteTxt] - self.konf.taster[self.konf.nameTaster[0]]
                sperre = self.taster[sperrIndex]
                print(tasteTxt + ' gedrückt')
                if sperre:
                    # Sperre herausnehmen
                    self.taster[sperrIndex] = False
                    # Muster verstellen
                    func1()
                    # Led aus
                    self.i2c.aus(self.konf.Led[tasteTxt][0], self.konf.Led[tasteTxt][1])
                    Anlage.AnlagenStatus.anlageStatus[self.konf.taster[tasteTxt]] = False  # Led-Status in globaler Variabler
                    br.update_schalter()
                else:
                    # Sperre setzen
                    # Taste halten über Merker
                    self.taster[sperrIndex] = True
                    # Muster verstellen
                    func2()
                    # Led setzen
                    self.i2c.ein(self.konf.Led[tasteTxt][0], self.konf.Led[tasteTxt][1])
                    Anlage.AnlagenStatus.anlageStatus[self.konf.taster[tasteTxt]] = True  # Led-Status in globaler Variabler
                    br.update_schalter()

    @staticmethod
    def regen1_4():
        for i in range(0, 4):
            Anlage.AnlagenStatus.muster[i] = True

    @staticmethod
    def sperre1_4():
        for i in range(0, 4):
            Anlage.AnlagenStatus.muster[i] = False

    @staticmethod
    def regen5_10():
        for i in range(4, 10):
            Anlage.AnlagenStatus.muster[i] = True

    @staticmethod
    def sperre5_10():
        for i in range(4, 10):
            Anlage.AnlagenStatus.muster[i] = False


if __name__ == "__main__":
    # Testen
    print('start')
    konf = Anlage.AnlagenKonfiguration
    stat = Anlage.AnlagenStatus
    wasserZeit = Zeitgeber.Zeitgeber(stat.tRegen, False, False)
    sperrZeit = Zeitgeber.Zeitgeber(konf.t_sperre, False, False)
    i2c = I2Cio
    rg = Regen(wasserZeit, sperrZeit)
    # Ausgänge löschen
    i2c.clearList(konf.ausList)
    while True:
        i2c.eingang()
        for i in range(1, konf.nBits+1):
            if i2c.statusEin(i):
                i2c.resetEingang()
                print('Eingang: ', i)
                if not rg.taster[Anlage.AnlagenKonfiguration.nameTaster.index('Start')]:
                    # Manuelle Bewässerung
                    if (1 <= i <= 10):
                        if i == rg.wasserPlatz:
                            print('Platz ', i, ' wird nicht mehr bewässert')
                            rg.wasser_ende()
                        else:
                            print('Platz ', i, ' wird bewässert')
                            rg.beregnung_ein(i, stat.tRegen)

        if wasserZeit.check():
            print('Wasser wird zugemacht')
            rg.wasser_ende()

        if sperrZeit.check():
            print('Sperrzeit ist abgelaufen')
            rg.beregnung_ende()


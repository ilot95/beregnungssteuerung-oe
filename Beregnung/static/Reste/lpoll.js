var prev_data = null;           // remember data fetched last time
var waiting_for_update = false; // are we currently waiting?
var LONG_POLL_DURATION = 60000; // how long should we wait? (msec)

/**
 * Load data from /data, optionally providing a query parameter read
 * from the #format select
 */
function load_data() {
    var url = '/data' ;
    $.ajax({ url:     url,
             success: function(data) {
                          display_data(data);
                          wait_for_update();
                      },
    });
    return true;
}
/**
 * Uses separate update notification and data providing URLs. Could be combined, but if
 * they're separated, the Python routine that provides data needn't be changed from
 * what's required for standard, non-long-polling web app. If they're combined, arguably
 * over-loads the purpose of the function.
 */
function wait_for_update() {
    if (!waiting_for_update) {
        waiting_for_update = true;
        $.ajax({ url: '/updated',
                 success:  function () {
                    load_data();    // if /update signals results ready, load them
     //               console.assert(false, 'lade neue Daten');
                 },
                 complete: function () {
                    waiting_for_update = false;
                    wait_for_update(); // if the wait_for_update poll times out, rerun
     //               console.assert(false, 'weitere Runde');
                 },
                 timeout:  function () {
                    LONG_POLL_DURATION;
     //               console.assert(false, 'LONG_POLL_DURATION');
                 }
                 ,
               });
    }
    // wait_for_update guard to ensure not re-entering already running wait code
    // added after user suggestion. This check has not been needed in my apps
    // and testing, but concurrency is an area where an abundance of caution is
    // often the best policy.
}
/**
 * show the data acquired by load_data()
 */
function display_data(data) {
    if (data && (data != prev_data)) {      // if there is data, and it's changed
        // update the contents of several HTML divs via jQuery
        $('#help').html(data.zeit);
        document.getElementById('ein').value = data.zeit
        var skip = 0;   // fehlende Elemente werden übersprungen
        for(var i=0; i<16; i++ ){
            try {
                var temp = document.getElementById(i.toString()).style;
 //               console.assert(false, temp.backgroundColor, ' ', skip);
            }
            catch(e){
                console.assert(false, 'i= ', i, ' Meldung:  ', e);
                skip = 1;
            }
            if (skip == 0) {        // Hintergrundfarben werden entsprechend des Bool-Arrays data.format eingestellt
                if (data.format[i]) { temp.backgroundColor = "rgb(82, 177, 255)"} else { temp.backgroundColor = 'white'};
  //              console.assert(false, 'neue Farbe:  ', temp.backgroundColor);
            }
            else {
                skip = 0;
            }
        }
        // remember this data, in case want to compare it to next update
        prev_data = data;
    }
}
/**
 * Initial document setup -  provide a "loading..." message
 */
$(document).ready(function() {
    //$('div#contents').append('awaiting data...');
    // load the initial data (assuming it will be immediately available)
    load_data();
    // start periodic function: wenn update hängenbleibt, werden spätestens nach 5s neue Daten geholt
    var intervalID = setInterval(function(){
        // alert("Interval reached"); sendet an Webseite
        load_data();
        }
        , 5000);    // 5000ms
});
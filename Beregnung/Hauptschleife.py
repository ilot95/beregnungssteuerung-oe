# -*- coding: utf-8 -*-
from Beregnung import Anlage
from Beregnung import I2Cio
from Beregnung import Zeitgeber
from Beregnung import LogDatei as lg
from Beregnung import WatchDog
from Beregnung import Regen
import threading
import time
import datetime
import Beregnung as br


# das Beregnungsprogramm: daraus ist eine Klasse geworden
# ********************************************************
# Parameter werden in 'Anlage.py' eingestellt
class Hauptschleife():



    def run(self):
        konf = Anlage.AnlagenKonfiguration
        stat = Anlage.AnlagenStatus
        datei = konf.datei
        log = lg.LogDatei(datei)
        meld = 'Start der Steuerung'
        print(meld)
        log.write_log(meld)
        watchDog = WatchDog.WatchDog(konf.dateiWatch, datei)
        watch = Zeitgeber.Zeitgeber(konf.watch_zeit, True, True)
        printZeit = Zeitgeber.Zeitgeber(10, True, konf.debugprint)  # printer der Zustände für debug-Zwecke
        nochZeit = Zeitgeber.Zeitgeber(1, True, False)              # Zeitgeber für den Countdown
        i2c = I2Cio
        # Eingänge initialisieren
        i2c.resetEingang()
        # Ausgänge löschen
        i2c.clearList(konf.ausList)
        # Zeitgeber und Beregnungsobjekt initialisieren
        wasserZeit = Zeitgeber.Zeitgeber(stat.tRegen, False, False)       # so lange läuft das Wasser
        sperrZeit = Zeitgeber.Zeitgeber(konf.t_sperre, False, False)      # so lange ist die Pumpe gesperrt
        autoZeit = Zeitgeber.Zeitgeber(konf.wiederAuto, False, False)     # nach dieser Zeit ist Automatik wieder aktiv
        autoAktiv = True
        sperrEZeit = [0]    # wir fangen beim Index '1' an, damit werden die Eingänge gesperrt
        for i in range(1, konf.nBits+1):
            sperrEZeit.append(Zeitgeber.Zeitgeber(konf.tWart, False, False))
        rg = Regen.Regen(wasserZeit, sperrZeit, nochZeit)
        anz = 0
        if konf.performance:    # Performance Messung?
            start = time.time()

        current_module = __import__(__name__)
        while True:

            # corotine yield for eventlet
            current_module.socketio.sleep()

            # lese alle Eingänge **************************
            i2c.eingang()
            # Werte alle Eingänge aus
            for i in range(1, konf.nBits + 1):
                # hier greift das WEB-Interface über stat.aktion[i-1] wie eine parallel geschaltete Tastatur ein
                if i2c.statusEin(i) or stat.aktion[i-1]:    # gibt es eine Aktion?
                    # eine Aktion vom Eingang i ist fällig
                    i2c.resetEingang()                      # lösche weitere Aktionen von den Tastern
                    stat.aktion = [False] * konf.nBits      # lösche Aktionen vom Web-Interface
                    print('Eingang: ', i)
                    # die 10 Taster für die Plätze
                    if 1 <= i <= 10:
                        if not rg.tasterSperre[i]:
                            rg.tasterSperre[i] = True  # Sperre wird nach tWart wieder aufgehoben
                            # für jeden Eingang brauche ich einen eingenen Zeitgeber
                            sperrEZeit[i].start()
                            # Eingabe der Platzsperre ist aktiv
                            if stat.anlageStatus[konf.taster['Eingabe Sperre']]:
                                # Muster verändern als Umschalter
                                stat.muster_schalten(i)
                                # Anlagestatus nachführen: bei klaren Mustern bilden Sperre1_4 bzw. Sperre5_10
                                # den aktuellen Status ab, bei gemischten Zuständen bleiben die Tasten aus
                                stat.anlageStatus[konf.taster['Sperre 1-4']] = stat.muster_testen(1, 4)
                                stat.anlageStatus[konf.taster['Sperre 5-10']] = stat.muster_testen(5, 10)
                                # zeigen in diesem Modus das Muster auch an
                                br.update_muster()
                            # keine Automatik bzw. Start: rg.taster[0]=True - Einzelplatz wird ein- oder ausgeschaltet
                            elif not rg.taster[konf.nameTaster.index('Start')]:
                                rg.platz_schalten(i)
                    # Aktionen unabhängig vom Automatikbetrieb, komplett als Methode in Regen realisiert
                    rg.tastenAktion(i, 'Sperre 1-4', sperrEZeit, rg.regen1_4, rg.sperre1_4)
                    rg.tastenAktion(i, 'Sperre 5-10', sperrEZeit, rg.regen5_10, rg.sperre5_10)
                    # gesperrte Plätze eingeben
                    rg.tastenAktion(i, 'Eingabe Sperre', sperrEZeit, rg.leer, rg.leer) # schaltet um auf: Plätze sperren
                    # Automatik ein und aus
                    rg.tastenAktion(i, 'Sperre', sperrEZeit, rg.leer, rg.leer)  # sperrt Automatik
                    # Rundlauf ein und aus
                    # Muster abarbeiten, in Methode Regen.rundlauf() werden dazu die Parameter gesetzt
                    rg.tastenAktion(i, 'Start', sperrEZeit, rg.wasser_ende, rg.rundlauf)
                    break   # verlasse die Schleife

            # Aktionen ***********************************
            # Automatik bzw. Start: rg.taster[0]=True  nicht wenn Eingabe des Musters gewählt
            if rg.taster[konf.nameTaster.index('Start')] and not stat.anlageStatus[konf.taster['Eingabe Sperre']]:
                # Wasser steht wieder zur Verfügung
                if not rg.wasser:
                    # nicht alle Plätze abgearbeitet
                    if not Regen.Regen.musterFertig:
                        # starte Beregnung für den nächsten Platz wenn es das Muster zulässt
                        if stat.muster[rg.iPlatz-1]:
                            rg.beregnung_ein(rg.iPlatz, stat.tRegen)
                        # zähle den Platz hoch
                        rg.iPlatz += 1
                        if rg.iPlatz > konf.nPlaetze:
                            # fertig mit Durchlauf
                            Regen.Regen.musterFertig = True
                            rg.iPlatz = 1
                            rg.taster[konf.nameTaster.index('Start')] = False    # Taster zurücksetzen
                            # Led aus
                            i2c.aus(konf.Led['Start'][0], konf.Led['Start'][1])
                            # Led-Status in statischer Variable
                            stat.anlageStatus[konf.taster['Start']] = False
                            br.update_schalter()    # Webseiten aktualisieren

            # Aktionen der Zeitgeber *************************
            if wasserZeit.check():
                print('Wasser wird zugemacht')
                rg.wasser_ende()

            if sperrZeit.check():
                print('Sperrzeit für die Pumpe ist abgelaufen')
                rg.beregnung_ende()

            if autoZeit.check():
                print('Autozeit ist abgelaufen')
                autoAktiv = True

            for i in range(1, konf.nBits + 1):
                if sperrEZeit[i].check():
                    print('Wartezeit für Eingang: ', i, ' ist abgelaufen')
                    rg.tasterSperre[i] = False

            if printZeit.check():   # wird später ausgeblendet, Monitoring für gewisse Parameter
                print('Printer: ')
                temp = str(stat.anlageStatus)
                print(temp)

            if nochZeit.check():        # Countdown der Regen- und Sperrzeit eines Platzes
                stat.t_noch -= 1        # berechne die aktuelle Zeit
                print('Countdown: ', int(stat.t_noch))
                br.update_schalter()    # Webseiten aktualisieren

            if watch.check():
                watchDog.watch()

            # Tageszeit-Abfrage **************************
            # automatische Beregnung zu den gelisteten Zeiten,- prüfe auf Tageszeit aus der Liste der Bewässerungszeiten
            if not stat.anlageStatus[konf.taster['Sperre']] and autoAktiv:  # Automatik ist nicht gesperrt
                uhrzeit = datetime.datetime.now()
                for zeit in stat.autoZeit:
                    if zeit.hour == uhrzeit.hour:
                        if zeit.minute == uhrzeit.minute:
                            # starte den Rundlauf und sperre weitere Abfragen
                            stat.aktion[konf.taster['Start']] = True
                            autoZeit.start()
                            autoAktiv = False       # sperre diese Abfrage für eine gewisse Zeit (2min)
                            meld = 'Automatikbetrieb'
                            print(meld)
                            log.write_log(meld)     # später entfernen
                            break

            if konf.performance:        # Messung der Performance der Schleife *******************************
                anz += 1
                if anz > 1000:
                    anz = 0
                    zeit = time.time()
                    print ('Zeit für 1000 Durchgänge: ', zeit-start)
                    start = zeit


if __name__ == "__main__":
    schl = Hauptschleife()
    schl.run()

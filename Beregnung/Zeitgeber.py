import time


class Zeitgeber:
    # periodischer oder einmaliger Zeitgeber
    def __init__(self, zeit, periodisch, aktiv):
        self.zeit = zeit    # Periode
        self.periodisch = periodisch
        self.aktiv = aktiv
        self.startzeit = time.time()   # Startzeit

    def check(self):
        if self.aktiv and time.time() > self.startzeit + self.zeit:
            if self.periodisch:
                self.startzeit = time.time()  # Startzeit für periodischen Aufruf zurückstellen
            else:
                self.aktiv = False
            return True
        else:
            return False

    def start(self):        # starte den einmaligen Zeitgeber erneut
        self.startzeit = time.time()   # Startzeit
        self.aktiv = True


if __name__ == "__main__":
    zeit1 = Zeitgeber(10, True, True)
    zeit2 = Zeitgeber(3, False, True)
    zeit3 = Zeitgeber(22, False, True)
    test = 1
    if test == 1:
        while True:
            if zeit1.check():
                print('zeit 1')
            if zeit2.check():
                print('zeit 2')
            if zeit3.check():
                zeit2.start()
                print('zeit 3')

# -*- coding: utf-8 -*-

import os
import sys
import datetime as dt
from flask import Flask, request, jsonify, g, render_template, session, Response
# sys.path.append('/home/pi/.local/lib/python3.7/site-packages')
from flask_socketio import SocketIO, emit, disconnect
from .Hauptschleife import Hauptschleife
from . import Anlage
from Beregnung.auth import login_required


app = None
socketio = None
fehler = ''
# global, besser für Abfragen, Web-Oberflächen-Spezifische Konstanten
valueElement = ['Platz 1', 'Platz 2', 'Platz 3', 'Platz 4', 'Platz 5', 'Platz 6', 'Platz 7', 'Platz 8',
                'Platz 9', 'Platz 10', 'Rundlauf', 'Sperre1_4', 'Sperre5_10', "Sperre Automatik", "Eingabe Sperre"]
nameElement = ['platz1', 'platz2', 'platz3', 'platz4', 'platz5', 'platz6', 'platz7', 'platz8',
               'platz9', 'platz10', 'rundlauf', 'sperre1_4', 'sperre5_10', "sperre", "eingabsperre"]
idElement = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15]


def update_muster():   # kann als statische Methode von überall her aufgerufen werden
    global app, socketio, fehler, valueElement, idElement
    stat = Anlage.AnlagenStatus
    if stat.anlageStatus[idElement[valueElement.index("Eingabe Sperre")]]:
        print('Update_muster aufgerufen')
        if app is not None:
            with app.app_context():
                emit('update_muster', {'muster': stat.muster, 'regenzeit': str(stat.tRegen), 'fehler': fehler,
                                       'format': stat.anlageStatus}, namespace='', broadcast=True)


def update_schalter():   # kann als statische Methode von überall her aufgerufen werden
    global app, socketio, fehler
    stat = Anlage.AnlagenStatus
    if not stat.anlageStatus[idElement[valueElement.index("Eingabe Sperre")]]:
        print('Update_schalter aufgerufen')
        if app is not None:
            with app.app_context():
                emit('update_schalter', {'muster': stat.muster, 'regenzeit': str(stat.tRegen), 'fehler': fehler,
                                         'format': stat.anlageStatus, 't_noch': int(stat.t_noch)},
                     namespace='', broadcast=True)


def update_zeit():   # kann als statische Methode von überall her aufgerufen werden
    global app, socketio, fehler
    print('Update_zeit aufgerufen')
    if app is not None:
        with app.app_context():
            emit('update_zeit', {'zeit': stringzeit(Anlage.AnlagenStatus.autoZeit), 'regenzeit': str(Anlage.AnlagenStatus.tRegen),
                        'fehler': fehler, 'format': Anlage.AnlagenStatus.anlageStatus}, namespace='', broadcast=True)


def stringzeit(liste):
    strofliste = ''
    for zeit in liste:  # Liste von Zeiten in 'Anlage'
        strofliste += zeit.isoformat(timespec='minutes') + ' '  # String für das Textfeld
    return strofliste


# application factory function
def create_app(test_config=None):
    global app, socketio, fehler
    async_mode = None
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY=os.urandom(24),    # ??? wofür ???
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),  # Name der Datenbank mit Usern
    )
    socketio = SocketIO(app, async_mode=async_mode)
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)
    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # a simple page that says hello and gives status
    @app.route('/hello')
    def hello():
        #db.init_db()
        temp = str(Anlage.AnlagenStatus.anlageStatus)
        return 'Hallo Beregnung: ' + temp

    @app.route('/data', methods=['GET'])
    def data():
        # now = time.time()
        info = {'zeit': stringzeit(Anlage.AnlagenStatus.autoZeit), 'regenzeit': str(Anlage.AnlagenStatus.tRegen),
                'fehler': fehler, 'muster': Anlage.AnlagenStatus.muster, 'format': Anlage.AnlagenStatus.anlageStatus}
        return jsonify(info)

    @app.route('/', methods=['POST', 'GET'])
    def index():
        global fehler, valueElement, nameElement, idElement
        autozeit = stringzeit(Anlage.AnlagenStatus.autoZeit)
        regenzeit = str(Anlage.AnlagenStatus.tRegen / 60)  # in Minuten
        register = Anlage.AnlagenStatus.register
        fehler = ''   # !!
        if request.method == 'POST' and g.user is None:
            fehler = 'Login notwendig für Eingaben'

        @login_required     # post-requests nur von angemeldeten Usern
        def eingabe():
            global fehler, autozeit, regenzeit
            if request.method == 'POST':        # ok
                fehler = ''
                autozeit = request.form['autozeit']        # als String gelesen
                templ = autozeit.split()    # zerlegen und als Liste speichern
                print('Zeiten für Auto gelesen: ', autozeit)
                zeiten = []
                for i in range(0, len(templ)):
                    print('Zeit: ', templ[i])
                    try:
                        zeiten.append(dt.time.fromisoformat(templ[i]))
                    except:
                        # Meldung für die Webseite
                        meld = 'Zeit ' + str(i+1) + ' ist falsch; '
                        fehler += meld
                        print(meld)
                print(zeiten)
                Anlage.AnlagenStatus.autoZeit = zeiten
                regenzeit = request.form['regenzeit']
                Anlage.AnlagenStatus.tRegen = 60 * float(regenzeit)    # Sekunden werden intern verwendet
                if Anlage.AnlagenStatus.tRegen > 10*60:
                    Anlage.AnlagenStatus.tRegen = 10 * 60  # harte Begrenzung der Eingabe
                    fehler += 'Beregnungszeiten > 10min sind nicht erlaubt; '
                if Anlage.AnlagenStatus.tRegen < 1*60:
                    Anlage.AnlagenStatus.tRegen = 1 * 60
                    fehler += 'Beregnungszeiten < 1min sind nicht erlaubt: '
                print('Dauer einer Beregnung: ', regenzeit, 'min', Anlage.AnlagenStatus.tRegen, 's')
                for i in range(0, len(idElement)):      # Elemente aus Formular abfragen
                    if request.form.get(nameElement[i]) == valueElement[i]:
                        # hierüber wird die Aktion parallel zur Tastatur ausgelöst
                        Anlage.AnlagenStatus.aktion[idElement[i]] = True
        eingabe()
        # immer einen Broadcast schicken, damit alle auf dem Laufenden sind:
        update_zeit()
        if Anlage.AnlagenStatus.anlageStatus[idElement[valueElement.index("Eingabe Sperre")]]:
            update_muster()
        else:
            update_schalter()
        return render_template('status/index.html', **locals())

    @socketio.on('connect')
    def test_connect():
        print('Connected to Server')

    @socketio.on('disconnect')  # wird gelegentlich aufgerufen
    def test_disconnect():
        print('Client disconnected', request.sid)

    # Start der Steuerung in einem eigenen Thread
    hp = Hauptschleife()
    # hp.start()
    socketio.start_background_task(hp.run)

    from . import db    # User Datenbank
    db.init_app(app)

    from . import auth
    app.register_blueprint(auth.bp)

    return app

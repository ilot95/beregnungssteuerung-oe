from Beregnung import LogDatei
from Beregnung import Anlage
import smbus
import time
import random


# Ein- und Ausgangsfunktionen auf Byte-Basis und Hardware Adressen
# Lesen von Eingängen über mehrere Baugruppen hinweg unter Berücksichtigung von Entprellzeiten
konf = Anlage.AnlagenKonfiguration
statusAlt = [False] * konf.nBits
statusAktion = [False] * konf.nBits
zeiten = [0] * konf.nBits
log = LogDatei.LogDatei(konf.datei)

try:
    bus = smbus.SMBus(1)  # Bus-Objekt erzeugen
except Exception as e:
    text = 'Fehler beim Öffnen der Schnittstelle '
    print(text)
    log.write_log(text)
    text = "Exception: " + str(type(e)) + str(e)
    print(text)
    log.write_log(text)


def statusEin(nr: int):
    # Wertebereich um 1 verschoben
    return statusAktion[nr-1]


def lese(adres: int):        # lese ein Byte
    try:
        temp = bus.read_byte(adres)  # Wert von der Digital-Input-Karte lesen
    except Exception as e:
        text = 'Fehler beim Lesen der Adresse: ' + str(adres)
        print(text)
        log.write_log(text)
        text = "Exception: " + str(type(e)) + str(e)
        print(text)
        log.write_log(text)
        return 'err 2'
    return 255 - temp


def schreibe(adres: int, wert: int):      # schreibe ein Byte
    try:
        bus.write_byte(adres, 255 - wert)
    except Exception as e:
        text = 'Fehler beim Schreiben der Adresse: ' + str(adres) + ' mit Wert: ' + str(255-wert)
        print(text)
        log.write_log(text)
        text = "Exception: " + str(type(e)) + str(e)
        print(text)
        log.write_log(text)
        return 'err 1'
    return


def clear(adres: int):      # lösche ein Byte
    schreibe(adres, 0)
    return


def clearList(liste: int):      # lösche eine Liste von Adressen
    for i in range(0, len(liste)):
        clear(liste[i])
    return


def int_to_bool(num: int, anz: int):      # anz Bits werden in eine Liste mit bool umgesetzt
    return [bool(num & (1 << i)) for i in range(anz)]


def set_bit(value: int, bit: int):    # setze ein Bit in einem Integer 'value'
    return value | (1 << bit)


def clear_bit(value: int, bit: int):  # lösche ein Bit in einem Integer 'value'
    return value & ~(1 << bit)


def ein(adres: int, nummer: int):     # setze einen Ausgang (0-7) auf ein
    oByte = lese(adres)
    oByte = set_bit(oByte, nummer)
    schreibe(adres, oByte)


def aus(adres: int, nummer: int):    # setze einen Ausgang (0-7) zurück (aus)
    oByte = lese(adres)
    oByte = clear_bit(oByte, nummer)
    schreibe(adres, oByte)


def resetEingang(): # später in andere Datei !!!
    # verhindere alle anderen Aktionen, wenn eine Aktion abgearbeitet wird
    global statusAlt, statusAktion, zeiten
    statusAlt = [False] * konf.nBits
    statusAktion = [False] * konf.nBits
    zeiten = [0] * konf.nBits
    return


# lese den Eingangszustand und setzte den Aktionsstatus mit Entprellung (tPrell)
def eingang():
    global statusAlt, statusAktion, zeiten
    liste = []
    for i in range(0, len(konf.einAdressen)):
        clear(konf.einAdressen[i])     # alle Eingänge auf Null setzen, damit der aktuelle Zustand gelesen wird
        liste += (int_to_bool(lese(konf.einAdressen[i]), konf.bitsProBaugruppe))
    for j in range(0, konf.nBits):
        if liste[j] != statusAlt[j]:
            zeiten[j] = time.time()
            statusAlt[j] = liste[j]
        if (time.time() - zeiten[j]) > konf.tPrell and liste[j]:
            print('Aktion: auf bit: ', j)
            statusAktion[j] = liste[j]


if __name__ == "__main__":
    test = 1
    if test == 1:
        while True:
            j = random.randint(0, 255)
            print('auf Ausgang: ', j)
            schreibe(konf.adresseSchreiben1, j)
            schreibe(konf.adresseSchreiben2, j)
            schreibe(konf.adresseSchreibAC1, j)
            schreibe(konf.adresseSchreibAC2, j)
            schreibe(konf.adresseLesen1, j)
            schreibe(konf.adresseLesen2, j)
            time.sleep(2)

            clear(konf.adresseSchreiben1)

            for i in range(0, 8):
                ein(konf.adresseSchreiben1, i)
                time.sleep(0.2)
            for i in range(7, -1, -1):
                aus(konf.adresseSchreiben1, i)
                time.sleep(0.2)
    elif test == 2:
        while True:
            eingang()
            for i in range(0, konf.nBits):
                if statusAktion[i]:
                    resetEingang()
                    print('Eingang: ', i)

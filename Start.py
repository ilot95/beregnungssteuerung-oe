import Beregnung as br



if __name__ == '__main__':
    app = br.create_app()


    br.socketio.run(app, host='0.0.0.0', port=5000, debug=False)    # debug=True: server läuft 2-mal
